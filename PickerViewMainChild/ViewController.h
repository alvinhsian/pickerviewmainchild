//
//  ViewController.h
//  PickerViewMainChild
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/27.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>{
    
//    NSArray *dataArray;
    
    //第一個iVar儲存資料的
    NSMutableDictionary *data;
    //第二個iVar用來固定資料在Dictionary key的順序
    NSArray *keys;

}
@property (weak, nonatomic) IBOutlet UILabel *showLabel;

- (IBAction)toPick:(id)sender;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end

