//
//  ViewController.m
//  PickerViewMainChild
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/27.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    dataArray= [NSArray arrayWithObjects:@"炒飯",@"青菜",@"豆腐湯", nil];
    
    // 以Dictionary + Array的方式表達本次練習所用到的資料
    data = [[NSMutableDictionary alloc]init];
    [data setValue:@[@"炒飯",@"青菜",@"豆腐湯"] forKey:@"中式"];
    [data setValue:@[@"漢堡",@"薯條",@"可樂"] forKey:@"西式"];
    
    /* 
        因為上面的Dictionary裡頭的Key對上Value的值並沒有按照順序排列的，為了避免滾軸上面一下中式在前面或西式在前面，
        所以我們希望先把資料取出來確定排序後再放入另一個陣列之內，重點在於固定順序而非排序，不論中式或西式在前都好。
        在data(Dictionary)呼叫allKeys取出所有的key(中式, 西式)確定排列順序。
        呼叫sortedArrayUsingComparator排序的方法輸入兩個比較的對象
     */
    keys= [[data allKeys] sortedArrayUsingComparator:(NSComparator)^(id object1, id object2){
        return [object1 caseInsensitiveCompare:object2];
    } ];
    
}

//這段如果沒實作，pickView上面顯示的選項就會是一個個問號
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
//    return [dataArray objectAtIndex:row];
    
    
    // 如果是第0個滾軸(指定中式或西式)，就抓取第0個滾軸上哪一個被指定的row就是哪一個值(中式，西式)。
    if (component==0) {
        return [keys objectAtIndex:row];
    } else {
        // 1.指定由第0個滾軸決定選到中式或西式。
        NSString *key = [keys objectAtIndex:[_pickerView selectedRowInComponent:0]];
        
        // 2.再由第0個滾軸所取得的key決定第1個滾軸該顯示哪一個陣列。
        NSArray *array = [data objectForKey:key];
        
        // 3.確定哪個陣列後再由目前的列號決定取出第幾筆資料(也就是哪一種食物的意思)。
        return [array objectAtIndex:row];
    }
    
    
    
    
}
// 設定顯示在pickerView有幾個component
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerVie
{
    // 目前有兩個滾軸。
    return 2;
}

// 這個方法用來回傳一個值代表該component裡頭的rows
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
//    return [dataArray count];
    if (component==0) {
        //如果是第0個component(左邊)請他去問keys這個陣列即可
        return [keys count];
    }else{
        // 右邊則先確定目前第0個滾軸選到誰，在確定是哪個陣列，從陣列的數目決定
        NSString *key=[keys objectAtIndex:[_pickerView selectedRowInComponent:0]];
        NSArray *array = [data objectForKey:key];
        return [array count];
    }
}
// 當使用者在滾軸選取時就被觸發
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component==0) {
        // 當使用者正在切換主分類（中式，西式）就重新整理右邊滾軸
        return [_pickerView reloadComponent:1];
    } else {
        // 當使用者正在切換子分類，直接拿出key對應陣列的資料show到Label上
        NSString *key=[keys objectAtIndex:[_pickerView selectedRowInComponent:0]];
        NSArray *array = [data objectForKey:key];
        _showLabel.text = [array objectAtIndex:row];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// 這個按鈕暫時用不到先關閉，請到Main.storyboard將按鈕設定為隱藏。
- (IBAction)toPick:(id)sender {

//    _showLabel.text = [dataArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
    
}
@end
